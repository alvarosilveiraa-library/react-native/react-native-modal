import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TouchableOpacity,
  Animated,
  Easing,
  BackHandler,
  Dimensions
} from 'react-native';
import styles from './styles';

const SCREEN_WIDTH = Dimensions.get('window').width;

class ActionView extends React.Component {
  static defaultProps = {
    visible: true,
    action: true
  };

  static propTypes = {
    visible: PropTypes.bool,
    action: PropTypes.bool
  };

  render() {
    const {
      visible,
      action,
      style,
      onPress,
      children
    } = this.props;

    if(!visible) return null;

    if(action) {
      return (
        <TouchableOpacity
          style={style}
          activeOpacity={1}
          onPress={onPress}
        >
          {children}
        </TouchableOpacity>
      );
    }

    return (
      <View style={style}>
        {children}
      </View>
    );
  }
}

export default class Modal extends React.Component {
  static defaultProps = {
    visible: false,
    dismiss: false,
    padding: 24,
    overlayColor: 'rgba(0, 0, 0, 0.8)'
  };

  static propTypes = {
    visible: PropTypes.bool,
    dismiss: PropTypes.bool,
    padding: PropTypes.number,
    overlayColor: PropTypes.string
  };

  state = {
    closing: true,
    visible: false,
    dismiss: this.props.dismiss,
    padding: this.props.padding,
    overlayColor: this.props.overlayColor,
    children: this.props.children
  };

  _queue = [];

  _animation = new Animated.Value(0);

  _handleBackPress = () => {
    const { visible, dismiss } = this.state;

    if(visible && dismiss) this.closeAsync();

    return true;
  };

  _addBackPress = () => BackHandler.addEventListener('hardwareBackPress', this._handleBackPress);

  _removeBackPress = () => BackHandler.removeEventListener('hardwareBackPress', this._handleBackPress);

  componentDidMount() {
    if(this.props.visible) {
      this.setState({ visible: true }, () => {
        this._addBackPress();

        this._animate(1);
      })
    }
  }

  componentDidUpdate(prevProps) {
    const visible = this.props.visible;

    if(prevProps.visible !== visible) {
      if(visible) {
        this.setState({ visible: true }, () => {
          this._addBackPress();

          this._animate(1);
        });
      }else {
        this.setState({ visible: false }, () => {
          this._removeBackPress();

          this._animate(0);
        });
      }
    }
  }

  _animate(value, onclosed) {
    Animated.timing(
      this._animation,
      {
        toValue: value,
        duration: 300,
        easing: Easing.ease
      }
    ).start(() => {
      if(
        onclosed &&
        typeof onclosed === 'function'
      ) setTimeout(onclosed, 20);

      if(!value) {
        this._removeBackPress();

        this.setState({ visible: false, overlayColor: null });
      }
    });
  }

  openAsync(children=null, {
    dismiss=false,
    padding=24,
    overlayColor=null
  }={}) {
    return new Promise(resolve => {
      if(this.state.visible) {
        this._queue.push({
          children,
          dismiss,
          padding,
          overlayColor
        });

        resolve();

        return;
      }

      this.setState({
        closing: false,
        visible: true,
        children,
        dismiss,
        padding,
        overlayColor
      }, () => {
        this._addBackPress();

        this._animate(1, () => resolve());
      });
    });
  }

  closeAsync() {
    return new Promise(resolve => {
      this.setState({ closing: true }, () => {
        this._animate(0, () => {
          if(this._queue.length) {
            const config = this._queue.splice(0, 1)[0];

            this.openAsync(config.children, config);
          }

          resolve();
        });
      });
    });
  }

  render() {
    const {
      closing,
      visible,
      dismiss,
      padding,
      children,
      overlayColor
    } = this.state;

    const opacityStyle = {
      opacity: this._animation.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1],
      })
    }

    const scaleUpStyle = {
      transform: [
        {
          scale: this._animation.interpolate({
            inputRange: [0, 1],
            outputRange: [1.1, 1],
          })
        }
      ]
    }

    const translateXStyle = {
      transform: [
        {
          translateX: this._animation.interpolate({
            inputRange: [0, 1],
            outputRange: [SCREEN_WIDTH, 0],
          })
        }
      ]
    }

    return (
      <ActionView
        visible={visible}
        action={dismiss}
        style={styles.overlay}
        onPress={() => this.close()}
      >
        <Animated.View style={[
          styles.container,
          opacityStyle,
          {
            padding,
            backgroundColor: overlayColor
          }
        ]}>
          <Animated.View style={[
            styles.content,
            !padding && closing
              ? translateXStyle
              : scaleUpStyle,
            padding
              ? { maxHeight: '100%' }
              : { height: '100%' },
          ]}>
            {children}
          </Animated.View>
        </Animated.View>
      </ActionView>
    );
  }
}
