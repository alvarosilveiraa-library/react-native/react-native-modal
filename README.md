# react-native-modal
> React Native Modal for Android and iOS

## Getting started
`$ npm i react-native-modal --save`

## Usage
```javascript
import React from 'react';
import {
  View,
  Text
} from 'react-native';
import Modal from 'react-native-modal';

// COMPONENT
<Modal
  ref={ref => { global.Modal = ref; }}
  visible={true}
  dismiss={true}
  padding={0}
  overlayColor="#000000"
/>

const modal = global.Modal; // GLOBAL REF (NOT REQUIRED)

// FUNCTION TO OPEN MODAL
modal.openAsync((
  <View style={{
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: 200,
    backgroundColor: '#FFFFFF'
  }}>
    <Text>MINHA MODAL</Text>
  </View>
), {
  dismiss: true,            // DEFAULT false
  padding: 0,               // DEFAULT 0
  overlayColor: '#000000'   // DEFAULT 'rgba(0, 0, 0, 0.8)'
}); // RESOLVE PROMISE WHEN ANIMATION FINISHED

// FUNCTION TO CLOSE MODAL
modal.closeAsync(); // RESOLVE PROMISE WHEN ANIMATION FINISHED
```
